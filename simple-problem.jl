#***************************************************************************************#
#*----- Auteur :        Aubertin Emmanuel                                           ****#
#*----- Description :   Simple template for linear programming                      ****#
#*----- Contact :       https://athomisos.fr                                        ****#
#***************************************************************************************#

#-------------------------------------------------
# Problem description :
#-------------------------------------------------
#  Variables :
#    a >= 0
#    b >= 0
#  Objective Function :
#    f = max 4a + 5b
#  Constraint :
#    2a + 5b <= 800
#    a + 2b >= 70
#    b <= 300 

using Cbc

#-------------------------------------------------
#  Function for split section in console
function  PrintSplit(SplitName)
    println()
    println("--------------------| \x1b[1m",SplitName,"\x1b[0m |--------------------")
    println()
end

#-------------------------------------------------
#  Function for print Done in green
function PrintDone()
    println("\x1b[32mDone\x1b[0m")
end

#-------------------------------------------------
#  Init Model
 PrintSplit("Model in Creation")
MyModel = Model(Cbc.Optimizer)
PrintDone()

#-------------------------------------------------
#  Variables
 PrintSplit("Initialisatin of variable")
@variable(MyModel, a >= 0, Int)
@variable(MyModel, b >= 0, Int)
PrintDone()

#-------------------------------------------------
#  Function objective
 PrintSplit("Declation of objective")
@objective(MyModel, Max, 4a + 5b )
PrintDone()

#-------------------------------------------------
#  constraint
 PrintSplit("Constraint in creation")
@constraint(MyModel, 2a + 5b <= 800 )
@constraint(MyModel, a + 2b >= 70 )
@constraint(MyModel, b <= 300 )
PrintDone()

#-------------------------------------------------
#  Run solution
 PrintSplit("Resolution of the Model")
status = optimize!(MyModel)
PrintDone()

#-------------------------------------------------
#  Get result
 PrintSplit("Store Result")
OutMa = value(a)
OutMb = value(b)
PrintDone()

#-------------------------------------------------
#  Show Resultb
PrintSplit("Show results")
print("Model info :\n", myMod)

println("\nSolve time :", solve_time(myMod))
println("Objective value : ", objective_value(myMod))
println("\nSolution is:")
println("    a = ", OutMa)
println("    b = ", OutMb)
PrintDone()

 PrintSplit("Good bye")
