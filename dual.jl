#***************************************************************************************#
#*----- Auteur :        Aubertin Emmanuel                                           ****#
#*----- Description :   Simple template for linear programming                      ****#
#*----- Contact :       https://athomisos.fr                                        ****#
#***************************************************************************************#

#-------------------------------------------------
# Problem description :
#-------------------------------------------------
#  Variables :
#    a >= 0
#    b >= 0
#  Objective Function :
#    f = max 1000a + 1200b
#  Constraint :
#    2a <= 1000
#    4a + 3b <= 3000
#    2a + 3b <= 2401 

using JuMP
using GLPK

#-------------------------------------------------
#  Function for split section in console
function  PrintSplit(SplitName)
    println()
    println("--------------------| \x1b[1m",SplitName,"\x1b[0m |--------------------")
    println()
end

#-------------------------------------------------
#  Function for print Done in green
function PrintDone()
    println("\x1b[32mDone\x1b[0m")
end

#-------------------------------------------------
#  Init Model
PrintSplit("Model in Creation")
myMod = Model(GLPK.Optimizer)
PrintDone()

#-------------------------------------------------
#  Variables
PrintSplit("Initialisatin of variable")
@variable(myMod, a >= 0) #milliers de Everlast produits
@variable(myMod, b >= 0) #milliers de Xeros produits
PrintDone()

#-------------------------------------------------
#  Function objective
PrintSplit("Declation of objective")
@objective(myMod, Max, 1000a + 1200b)
PrintDone()

#-------------------------------------------------
#  constraint
PrintSplit("Constraint in creation")
@constraint(myMod, C1 , 2a <= 1000)
@constraint(myMod, C2 , 4a + 3b <= 3000)
@constraint(myMod, C3 , 2a + 3b <= 2401 )
PrintDone()

#-------------------------------------------------
#  Run solution
PrintSplit("Resolution of the Model")
status = optimize!(myMod)
PrintDone()


#-------------------------------------------------
#  Get result
PrintSplit("Store Result")
Outa = value(a)
Outb = value(b)
PrintDone()

#-------------------------------------------------
#  Show results
PrintSplit("Show results")
print("Model info :\n", myMod)

println("\nSolve time :", solve_time(myMod))
println("Objective value : ", objective_value(myMod))
println("\nSolution is:")
println("    a = ", Outa)
println("    b = ", Outb)
println("\nOptimal value for dual :")
println("    y1 = ", dual(C1))
println("    y2 = ", dual(C2))
println("    y3 = ", dual(C3))

PrintSplit("Good bye")